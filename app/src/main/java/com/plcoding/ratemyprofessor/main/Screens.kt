package com.plcoding.ratemyprofessor.main

// Screens.kt

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.rounded.Star
import androidx.compose.material3.Button
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavController
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.rememberLottieComposition



@Composable
fun HomeScreen() {
    val composition by rememberLottieComposition(
        LottieCompositionSpec.RawRes(R.raw.ratemyprofessor)
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top // Align elements to the top
    ) {
        Box(
            modifier = Modifier
                .size(300.dp) // Set the desired size for the LottieAnimation
                .padding(top = 200.dp, bottom = 0.dp), // Adjust top padding to position it 6dp above
            contentAlignment = Alignment.Center
        ) {
            // Lottie animation centered within the Box
            LottieAnimation(composition = composition)
        }

        Text(
            text = "Welcome to Rate My Professor",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.onBackground
        )

        Spacer(modifier = Modifier.height(16.dp))

        Text(
            text = "Find and rate your professors!",
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.onBackground
        )

        Spacer(modifier = Modifier.height(16.dp))

        Text(
            text = "Get ready to shape your academic journey! With Rate my Professor, you can rate your professors and add them to your personal catalog. Share your insights, discover top educators, and curate your academic adventure! Start now and take control of your learning experience. \uD83C\uDF93✨",
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.onBackground
        )
    }
}


@Composable
fun ProfileScreen(navController: NavController, users: List<UserDetails>, professors: List<UserDetails>) {
    Column(
        modifier = Modifier.fillMaxSize(),
    ) {
        Spacer(modifier = Modifier.height(80.dp))

        Box(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f)
        ) {
            LazyColumn(
                modifier = Modifier.fillMaxSize()
            ) {
                // Use the combined list of users and professors for items
                items(users + professors) { user ->
                    UserItem(user = user) { user ->
                        // Navigate to the DetailsScreen with the user's ID
                        navController.navigate("details/${user.id}")
                    }
                }

                item {
                    Spacer(modifier = Modifier.height(80.dp))
                }
            }

            FloatingActionButton(
                onClick = {
                    navController.navigate("add_professor")
                },
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        imageVector = Icons.Default.Add,
                        contentDescription = "Add",
                        modifier = Modifier.size(24.dp)
                    )
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(
                        text = "Add Professor",
                        style = LocalTextStyle.current.copy(color = MaterialTheme.colorScheme.onBackground)
                    )
                }
            }
        }
    }
}

@Composable
fun AddProfessorScreen(
    navController: NavController,
    users: List<UserDetails>,
    professors: MutableList<UserDetails>
) {
    var professorName by remember { mutableStateOf("") }
    var professorEmail by remember { mutableStateOf("") }
    var selectedRating by remember { mutableIntStateOf(0) }
    var professorCourses by remember { mutableStateOf("") }

    // Calculate the next available ID
    val nextId = (users + professors).maxByOrNull { it.id }?.id?.plus(1) ?: 1

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Top
    ) {
        Spacer(modifier = Modifier.height(120.dp))

        Text(
            text = "Add Professor",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier.padding(bottom = 16.dp)
        )

        TextField(
            value = professorName,
            onValueChange = { newValue -> professorName = newValue },
            label = { Text(text = "Professor Name") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 12.dp)
        )

        TextField(
            value = professorEmail,
            onValueChange = { newValue -> professorEmail = newValue },
            label = { Text(text = "Professor Email") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
        )

        // Courses Input (Comma-separated)
        TextField(
            value = professorCourses,
            onValueChange = { newValue -> professorCourses = newValue },
            label = { Text(text = "Enter courses (comma-separated)") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
        )

        Text(
            text = "Professor Rating",
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier.padding(bottom = 8.dp)
        )

        RatingBar(
            rating = selectedRating,
            onRatingChanged = { newRating ->
                selectedRating = newRating
            }
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Button(
                onClick = {
                    navController.popBackStack()
                },
                modifier = Modifier.weight(1f)
            ) {
                Text(text = "Back")
            }

            Spacer(modifier = Modifier.width(16.dp))

            Button(
                onClick = {
                    // Check if the professor's name is not empty and the rating is not 0
                    if (professorName.isNotEmpty() && selectedRating > 0) {
                        // Create a new UserDetails object with the calculated next available ID
                        val newProfessor = UserDetails(
                            id = nextId, // Use the next available ID
                            name = professorName,
                            email = professorEmail,
                            img = Icons.Default.Person,
                            rating = selectedRating, // Set the rating
                            courses = professorCourses.split(",").map { it.trim() } // Parse courses
                        )
                        professors.add(newProfessor) // Add the new professor to the list

                        // Navigate back to the ProfileScreen
                        navController.popBackStack()
                    }
                },
                modifier = Modifier.weight(1f)
            ) {
                Text(text = "Submit")
            }
        }
    }
}



@Composable
fun UserItem(user: UserDetails, onItemClick: (UserDetails) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(5.dp)
            .clickable { onItemClick(user) },
        verticalAlignment = Alignment.CenterVertically
    ) {
        // Display the user's image
        Icon(
            imageVector = user.img,
            contentDescription = null,
            modifier = Modifier.size(32.dp)
        )

        Spacer(modifier = Modifier.width(16.dp))

        // Display the user's name and email
        Column {
            Text(
                text = user.name,
                fontSize = 18.sp,
                fontWeight = FontWeight.Bold,
                color = MaterialTheme.colorScheme.onBackground
            )
            // Display courses for professors, if available
            if (user.courses.isNotEmpty()) {
                Text(
                    text = "Courses: ${user.courses.joinToString(", ")}",
                    fontSize = 14.sp,
                    color = MaterialTheme.colorScheme.onBackground
                )
            }

            // Display the rating in stars
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                for (i in 1..5) {
                    Icon(
                        imageVector = if (i <= user.rating) Icons.Default.Star else Icons.Rounded.Star,
                        contentDescription = null,
                        tint = if (i <= user.rating) MaterialTheme.colorScheme.primary else Color.Gray,
                        modifier = Modifier.size(16.dp)
                    )
                }
            }
        }
    }
}

@Composable
fun DetailsScreen(
    userOrProfessorId: String,
    users: List<UserDetails>,
    professors: List<UserDetails>,
    navController: NavController,
    selectedWebsite: Website
) {
    // Find the user or professor based on the ID
    val userDetails = (users + professors).find { it.id.toString() == userOrProfessorId }

    // Extract first name and last name
    val fullName = userDetails?.name.orEmpty()
    val nameParts = fullName.split(" ")
    val firstName = nameParts.getOrNull(0).orEmpty()
    val lastName = nameParts.getOrNull(1).orEmpty()

    // Create a URL string with the first name and last name based on the selected website
    val searchUrl = when (selectedWebsite) {
        Website.AAU -> {
            // Use the first name and last name for AAU website
            "${selectedWebsite.searchUrl}${firstName}%20${lastName}"
        }
        else -> {
            // Use the default search URL for other websites
            "${selectedWebsite.searchUrl}${firstName}+${lastName}"
        }
    }

    val context = LocalContext.current // Retrieve the context

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = "Details",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier.padding(bottom = 16.dp)
        )

        Text(
            text = "Name: ${userDetails?.name}",
            fontSize = 18.sp,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier.padding(bottom = 8.dp)
        )

        Text(
            text = "Email: ${userDetails?.email}",
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier.padding(bottom = 8.dp)
        )

        Text(
            text = "Courses:",
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.onBackground
        )

        Spacer(modifier = Modifier.width(8.dp))

        // Courses list as comma-separated text
        Text(
            text = userDetails?.courses?.joinToString(", ") ?: "",
            fontSize = 14.sp,
            color = MaterialTheme.colorScheme.onBackground
        )

        // Display the rating in stars
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(bottom = 8.dp, top = 16.dp)
        ) {
            Text(
                text = "Rating: ",
                fontSize = 16.sp,
                color = MaterialTheme.colorScheme.onBackground
            )
            RatingBar(
                rating = userDetails?.rating ?: 0,
                onRatingChanged = {}
            )
        }

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 10.dp)
        ) {
            // Back Button (On the Right)
            Button(
                onClick = {
                    navController.popBackStack()
                },
                modifier = Modifier
                    .weight(1f) // Take half of the available space
            ) {
                Text(text = "Back")
            }
            // Add a gap between the buttons
            Spacer(modifier = Modifier.width(110.dp))

            // Open Website Button (On the Left)
            Button(
                onClick = {
                    // Open the website URL in a browser when the button is clicked
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(searchUrl))
                    context.startActivity(intent)
                },
                modifier = Modifier
                    .weight(1f) // Take half of the available space
            ) {
                // Add an image vector from the drawable folder
                Icon(
                    painter = painterResource(id = R.drawable.globe),
                    contentDescription = "Open in a new window",
                    modifier = Modifier.size(24.dp)
                )
            }
        }

        // Edit Professor Button (Only for professors)
        if (userDetails in professors) {
            Button(
                onClick = {
                    // Navigate to the EditProfessorScreen with the professor's ID
                    navController.navigate("edit_professor/${userDetails?.id}")
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 16.dp)
            ) {
                Text(text = "Edit Professor")
            }
        }
    }
}

enum class Website(val displayName: String, val searchUrl: String) {
    AU("AU", "https://www.au.dk/om/organisation/navnlist?inputSearch="),
    SDU("SDU", "https://www.sdu.dk/da/service/findperson?name="),
    AAU("AAU", "https://www.search.aau.dk/#gsc.tab=0&gsc.q=")
}

@Composable
fun SettingsScreen(
    selectedWebsite: Website,
    onWebsiteSelected: (Website) -> Unit
) {
    var showDialog by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(vertical = 60.dp, horizontal = 16.dp),
    ) {
        Text(
            text = "Settings",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier.padding(bottom = 16.dp)
        )
        Text(
            text = "Select a University",
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.onBackground)
        Spacer(modifier = Modifier.height(16.dp))

        // Show selected website
        Text("Chosen University: ${selectedWebsite.displayName}")

        Spacer(modifier = Modifier.height(16.dp))

        // Button to open website selection dialog
        Button(
            onClick = { showDialog = true },
            modifier = Modifier.fillMaxWidth()
        ) {
            Text("Select University")
        }

        if (showDialog) {
            WebsiteSelectionDialog(
                selectedWebsite = selectedWebsite,
                onWebsiteSelected = { website ->
                    onWebsiteSelected(website) // Call the callback function with the selected website
                    showDialog = false
                }
            )
        }
    }
}



@Composable
fun WebsiteSelectionDialog(
    selectedWebsite: Website,
    onWebsiteSelected: (Website) -> Unit
) {
    val websiteOptions = Website.values()

    Dialog(
        onDismissRequest = { /* Dismiss the dialog */ },
        properties = DialogProperties(
            dismissOnClickOutside = true,
            dismissOnBackPress = true
        )
    ) {
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Text(
                text = "Select a Website",
                color= MaterialTheme.colorScheme.onPrimary,
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(bottom = 16.dp)
            )

            websiteOptions.forEach { website ->
                val isSelected = website == selectedWebsite
                val textColor = if (isSelected) MaterialTheme.colorScheme.primary else Color.White

                Text(
                    text = website.displayName,
                    color = textColor,
                    modifier = Modifier
                        .fillMaxWidth()
                        .clickable {
                            onWebsiteSelected(website)
                        }
                        .padding(vertical = 8.dp),
                )
            }
        }
    }
}
@Composable
fun EditProfessorScreen(
    professorId: String,
    professors: MutableList<UserDetails>,
    onEditProfessor: (UserDetails) -> Unit,
    navController: NavController
) {
    // Find the professor based on the ID
    val professor = professors.find { it.id.toString() == professorId }

    // Create mutable variables to hold the edited details
    var editedProfessorName by remember { mutableStateOf(professor?.name.orEmpty()) }
    var editedProfessorEmail by remember { mutableStateOf(professor?.email.orEmpty()) }
    var editedSelectedRating by remember { mutableIntStateOf(professor?.rating ?: 0) }
    var editedCourses by remember { mutableStateOf(professor?.courses?.joinToString(", ").orEmpty()) }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Top
    ) {
        Spacer(modifier = Modifier.height(120.dp))

        Text(
            text = "Edit Professor",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier.padding(bottom = 16.dp)
        )

        TextField(
            value = editedProfessorName,
            onValueChange = { newValue -> editedProfessorName = newValue },
            label = { Text(text = "Professor Name") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 12.dp)
        )

        TextField(
            value = editedProfessorEmail,
            onValueChange = { newValue -> editedProfessorEmail = newValue },
            label = { Text(text = "Professor Email") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
        )

        // Courses Input (Comma-separated)
        TextField(
            value = editedCourses,
            onValueChange = { newValue -> editedCourses = newValue },
            label = { Text(text = "Enter courses (comma-separated)") },
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 16.dp)
        )

        Text(
            text = "Professor Rating",
            fontSize = 16.sp,
            color = MaterialTheme.colorScheme.onBackground,
            modifier = Modifier.padding(bottom = 8.dp)
        )

        RatingBar(
            rating = editedSelectedRating,
            onRatingChanged = { newRating ->
                editedSelectedRating = newRating
            }
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Button(
                onClick = {
                    // Navigate back to the ProfilesScreen
                    navController.navigate("profile")
                },
                modifier = Modifier.weight(1f)
            ) {
                Text(text = "Cancel")
            }

            Spacer(modifier = Modifier.width(16.dp))

            Button(
                onClick = {
                    // Create a copy of the professor with the edited details
                    val editedProfessor = professor?.copy(
                        name = editedProfessorName,
                        email = editedProfessorEmail,
                        rating = editedSelectedRating,
                        courses = editedCourses.split(",").map { it.trim() }.toMutableList() // Parse courses
                    )

                    // Call the onEditProfessor callback to update the professor in the list
                    editedProfessor?.let { onEditProfessor(it) }

                    // Navigate back to the DetailsScreen
                    navController.navigate("profile")
                },
                modifier = Modifier.weight(1f)
            ) {
                Text(text = "Save")
            }
        }
    }
}

@Composable
fun RatingBar(
    rating: Int,
    onRatingChanged: (Int) -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically
    ) {
        for (i in 1..5) {
            Icon(
                imageVector = if (i <= rating) Icons.Default.Star else Icons.Rounded.Star,
                contentDescription = null,
                tint = if (i <= rating) MaterialTheme.colorScheme.primary else Color.Gray,
                modifier = Modifier
                    .size(50.dp) // Increase the size of the stars here
                    .clickable {
                        onRatingChanged(i)
                    }
            )
        }
    }
}



