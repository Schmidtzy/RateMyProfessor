package com.plcoding.ratemyprofessor.main
import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.NavigationDrawerItemDefaults
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.launch


@ExperimentalMaterial3Api
class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val items = navigationItems
            val professors = remember { mutableStateListOf<UserDetails>() } // Create a list to store professors
            val navController = rememberNavController()

            Surface(
                modifier = Modifier.fillMaxSize(),
                color = MaterialTheme.colorScheme.background
            ) {
                val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
                val scope = rememberCoroutineScope()
                val selectedItemIndex = remember { mutableIntStateOf(0) }

                ModalNavigationDrawer(
                    drawerContent = {
                        ModalDrawerSheet {
                            Spacer(modifier = Modifier.height(16.dp))
                            items.forEachIndexed { index, item ->
                                NavigationDrawerItem(
                                    label = {
                                        Text(text = item.title)
                                    },
                                    selected = index == selectedItemIndex.intValue,
                                    onClick = {
                                        when (item.title) {
                                            "Home" -> {
                                                navController.navigate("home")
                                            }
                                            "Profiles" -> {
                                                navController.navigate("profile")
                                            }
                                            "Settings" -> {
                                                navController.navigate("settings")
                                            }
                                            else -> {
                                                // Handle the case where a user is selected
                                                val user = users.find { it.name == item.title }
                                                user?.let { selectedUser ->
                                                    navController.navigate("details/${selectedUser.id}")
                                                }
                                            }
                                        }
                                        selectedItemIndex.intValue = index
                                        scope.launch {
                                            drawerState.close()
                                            }
                                    },
                                    icon = {
                                        Icon(
                                            imageVector = if (index == selectedItemIndex.intValue) {
                                                item.selectedIcon
                                            } else item.unselectedIcon,
                                            contentDescription = item.title
                                        )
                                    },
                                    badge = {
                                        if (item.title == "Profiles") {
                                            // Calculate the badge count dynamically based on users and professors
                                            val badgeCount = users.size + professors.size
                                            if (badgeCount > 0) {
                                                Text(text = badgeCount.toString())
                                            }
                                        } else {
                                            // For other items, you can set badgeCount as usual
                                            item.badgeCount?.let { badgeCount ->
                                                Text(text = badgeCount.toString())
                                            }
                                        }
                                    },
                                    modifier = Modifier
                                        .padding(NavigationDrawerItemDefaults.ItemPadding)
                                )
                            }
                        }
                    },
                    drawerState = drawerState
                )
                {
                    Scaffold(
                        topBar = {
                            TopAppBar(
                                title = {
                                    Text(text = "Rate My Professor")
                                },
                                navigationIcon = {
                                    IconButton(onClick = {
                                        scope.launch {
                                            drawerState.open()
                                        }
                                    }) {
                                        Icon(
                                            imageVector = Icons.Default.Menu,
                                            contentDescription = "Menu"
                                        )
                                    }
                                }
                            )
                        },
                        content = {
                            val (selectedWebsite, setSelectedWebsite) = remember { mutableStateOf(
                                Website.AU
                            ) }

                            NavHost(navController, startDestination = "home") {
                                composable("home") {
                                    HomeScreen()
                                }
                                composable("profile") {
                                    ProfileScreen(navController, users, professors)
                                }
                                composable("settings") {
                                    SettingsScreen(
                                        selectedWebsite = selectedWebsite,
                                        onWebsiteSelected = { website ->
                                            setSelectedWebsite(website)
                                        }
                                    )
                                }
                                composable("add_professor") {
                                    AddProfessorScreen(navController, users, professors)
                                }
                                composable("edit_professor/{professorId}") { backStackEntry ->
                                    val professorId = backStackEntry.arguments?.getString("professorId")
                                    val professor = professors.find { it.id.toString() == professorId }
                                    professor?.let {
                                        EditProfessorScreen(
                                            professorId = professorId.orEmpty(),
                                            professors = professors,
                                            onEditProfessor = { editedProfessor ->
                                                // Update the professor in the list
                                                val index = professors.indexOfFirst { it.id == editedProfessor.id }
                                                if (index != -1) {
                                                    professors[index] = editedProfessor
                                                }
                                            },
                                            navController = navController
                                        )
                                    } ?: run {
                                        // Handle the case where the professor is not found
                                        Text(
                                            text = "Professor not found",
                                            fontSize = 24.sp,
                                            fontWeight = FontWeight.Bold,
                                            color = MaterialTheme.colorScheme.onBackground,
                                            modifier = Modifier.padding(16.dp)
                                        )
                                    }
                                }

                                composable("details/{id}") { backStackEntry ->
                                    val professorId = backStackEntry.arguments?.getString("id")
                                    val professor = professors.find { it.id.toString() == professorId }
                                    val userId = users.find { it.id.toString() == professorId }
                                    professor?.let {
                                        DetailsScreen(
                                            userOrProfessorId = professorId.orEmpty(),
                                            users = users,
                                            professors = professors,
                                            navController = navController,
                                            selectedWebsite = selectedWebsite // Pass the selected website here
                                        )
                                    } ?: run {
                                        // Handle the case where the professor is not found
                                        userId?.let {
                                            DetailsScreen(
                                                userOrProfessorId = professorId.orEmpty(),
                                                users = users,
                                                professors = professors,
                                                navController = navController,
                                                selectedWebsite = selectedWebsite // Pass the selected website here
                                            )
                                        } ?: run {
                                            // Handle the case where the user is not found
                                            Text(
                                                text = "User or Professor not found",
                                                fontSize = 24.sp,
                                                fontWeight = FontWeight.Bold,
                                                color = MaterialTheme.colorScheme.onBackground,
                                                modifier = Modifier.padding(16.dp)
                                            )
                                        }
                                    }
                                }
                            }
                        }
                    )
                }
            }
        }
    }
}
