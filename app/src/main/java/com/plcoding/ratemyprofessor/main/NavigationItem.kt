package com.plcoding.ratemyprofessor.main


import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.Person
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.ui.graphics.vector.ImageVector


data class NavigationItem(
    val title: String,
    val selectedIcon: ImageVector,
    val unselectedIcon: ImageVector,
    val badgeCount: Int? = null
)

val navigationItems = listOf(
    NavigationItem(
        title = "Home",
        selectedIcon = Icons.Filled.Home,
        unselectedIcon = Icons.Outlined.Home,
    ),
    NavigationItem(
        title = "Profiles",
        selectedIcon = Icons.Filled.Person,
        unselectedIcon = Icons.Outlined.Person,
        badgeCount = 45
    ),
    NavigationItem(
        title = "Settings",
        selectedIcon = Icons.Filled.Settings,
        unselectedIcon = Icons.Outlined.Settings,
    ),
)

data class UserDetails(
    val id: Int,
    val name: String,
    val email: String,
    val img: ImageVector,
    val rating: Int,
    val courses: List<String> // Add courses property
)



val users = listOf(
    UserDetails(id = 1, name = "Michel Vedel Howard", email = "michel.howard@ece.au.dk", img = Icons.Default.Person, rating = 5, courses = listOf("Android", "Kotlin", "Compose")),
    UserDetails(id = 2, name = "Martin Knudsen", email = "makn@ece.au.dk", img = Icons.Default.Person, rating = 5, courses = listOf("Android", "Kotlin", "Compose")),
    UserDetails(id = 3, name = "Lars Schmidt Hansen", email = "Lasch@au.dk", img = Icons.Default.Person, rating = 4, courses = listOf("Student")),
    UserDetails(id = 4, name = "Luke Skywalker", email = "luke.skywalker@rebelalliance.com", img = Icons.Default.Person, rating = 5, courses = listOf("The Force", "Lightsabers")),
    UserDetails(id = 5, name = "Princess Leia Organa", email = "princess.leia@rebelalliance.com", img = Icons.Default.Person, rating = 4, courses = listOf("Diplomacy", "Leadership")),
    UserDetails(id = 6, name = "Han Solo", email = "han.solo@millenniumfalcon.com", img = Icons.Default.Person, rating = 4, courses = listOf("Smuggling", "Piloting")),
    UserDetails(id = 7, name = "Barney Stinson", email = "barney@howimetyourmother.com", img = Icons.Default.Person, rating = 5, courses = listOf("Suit Up", "Legendary Stories")),
    UserDetails(id = 8, name = "Ted Mosby", email = "ted@architect.com", img = Icons.Default.Person, rating = 4, courses = listOf("Architecture", "Romance")),
    UserDetails(id = 9, name = "Michael Scott", email = "michael@dundermifflin.com", img = Icons.Default.Person, rating = 3, courses = listOf("Management", "Paper Sales")),
    UserDetails(id = 10, name = "Jim Halpert", email = "jim@dundermifflin.com", img = Icons.Default.Person, rating = 4, courses = listOf("Pranks", "Sales")),
    UserDetails(id = 11, name = "Rachel Green", email = "rachel@centralperk.com", img = Icons.Default.Person, rating = 5, courses = listOf("Fashion", "Coffee")),
    UserDetails(id = 12, name = "Ross Geller", email = "ross@paleontology.com", img = Icons.Default.Person, rating = 4, courses = listOf("Paleontology", "Divorce")),
    UserDetails(id = 13, name = "Monica Geller", email = "monica@chef.com", img = Icons.Default.Person, rating = 4, courses = listOf("Cooking", "Cleaning")),
    UserDetails(id = 14, name = "Chandler Bing", email = "chandler@sarcasm.com", img = Icons.Default.Person, rating = 4, courses = listOf("Sarcasm", "Jokes")),
    UserDetails(id = 15, name = "Phoebe Buffay", email = "phoebe@songs.com", img = Icons.Default.Person, rating = 5, courses = listOf("Music", "Spirituality")),
    UserDetails(id = 16, name = "Joey Tribbiani", email = "joey@actor.com", img = Icons.Default.Person, rating = 4, courses = listOf("Acting", "Food")),
    UserDetails(id = 17, name = "Dwight Schrute", email = "dwight@dundermifflin.com", img = Icons.Default.Person, rating = 3, courses = listOf("Sales", "Agriculture")),
    UserDetails(id = 18, name = "Pam Beesly", email = "pam@dundermifflin.com", img = Icons.Default.Person, rating = 4, courses = listOf("Reception", "Art")),
    UserDetails(id = 19, name = "Stanley Hudson", email = "stanley@dundermifflin.com", img = Icons.Default.Person, rating = 3, courses = listOf("Sales", "Crossword Puzzles")),
    UserDetails(id = 20, name = "Jan Levinson", email = "jan@corporate.com", img = Icons.Default.Person, rating = 2, courses = listOf("Corporate Affairs", "Music")),
    UserDetails(id = 21, name = "Phyllis Smith", email = "phyllis@dundermifflin.com", img = Icons.Default.Person, rating = 4, courses = listOf("Sales", "Knitting")),
    UserDetails(id = 22, name = "Angela Martin", email = "angela@dundermifflin.com", img = Icons.Default.Person, rating = 2, courses = listOf("Cats", "Accounting")),
    UserDetails(id = 23, name = "Stanley Hudson", email = "stanley@dundermifflin.com", img = Icons.Default.Person, rating = 3, courses = listOf("Sales", "Crossword Puzzles")),
    UserDetails(id = 24, name = "Jan Levinson", email = "jan@corporate.com", img = Icons.Default.Person, rating = 2, courses = listOf("Corporate Affairs", "Music")),
    UserDetails(id = 25, name = "Phyllis Smith", email = "phyllis@dundermifflin.com", img = Icons.Default.Person, rating = 4, courses = listOf("Sales", "Knitting")),
    UserDetails(id = 26, name = "Angela Martin", email = "angela@dundermifflin.com", img = Icons.Default.Person, rating = 2, courses = listOf("Cats", "Accounting")),
    UserDetails(id = 27, name = "Joey Tribbiani", email = "joey@actor.com", img = Icons.Default.Person, rating = 4, courses = listOf("Acting", "Food")),
    UserDetails(id = 28, name = "Dwight Schrute", email = "dwight@dundermifflin.com", img = Icons.Default.Person, rating = 3, courses = listOf("Sales", "Agriculture")),
    UserDetails(id = 29, name = "Pam Beesly", email = "pam@dundermifflin.com", img = Icons.Default.Person, rating = 4, courses = listOf("Reception", "Art")),
    UserDetails(id = 30, name = "Stanley Hudson", email = "stanley@dundermifflin.com", img = Icons.Default.Person, rating = 3, courses = listOf("Sales", "Crossword Puzzles")),
    UserDetails(id = 31, name = "Jan Levinson", email = "jan@corporate.com", img = Icons.Default.Person, rating = 2, courses = listOf("Corporate Affairs", "Music")),
    UserDetails(id = 32, name = "Phyllis Smith", email = "phyllis@dundermifflin.com", img = Icons.Default.Person, rating = 4, courses = listOf("Sales", "Knitting")),
    UserDetails(id = 33, name = "Angela Martin", email = "angela@dundermifflin.com", img = Icons.Default.Person, rating = 2, courses = listOf("Cats", "Accounting")),
    UserDetails(id = 34, name = "Rachel Green", email = "rachel@centralperk.com", img = Icons.Default.Person, rating = 5, courses = listOf("Fashion", "Coffee")),
    UserDetails(id = 35, name = "Ross Geller", email = "ross@paleontology.com", img = Icons.Default.Person, rating = 4, courses = listOf("Paleontology", "Divorce")),
    UserDetails(id = 36, name = "Monica Geller", email = "monica@chef.com", img = Icons.Default.Person, rating = 4, courses = listOf("Cooking", "Cleaning")),
    UserDetails(id = 37, name = "Chandler Bing", email = "chandler@sarcasm.com", img = Icons.Default.Person, rating = 4, courses = listOf("Sarcasm", "Jokes")),
    UserDetails(id = 38, name = "Phoebe Buffay", email = "phoebe@songs.com", img = Icons.Default.Person, rating = 5, courses = listOf("Music", "Spirituality")),
    UserDetails(id = 39, name = "Joey Tribbiani", email = "joey@actor.com", img = Icons.Default.Person, rating = 4, courses = listOf("Acting", "Food")),
)




