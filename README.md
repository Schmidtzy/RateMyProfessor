# Rate My Professor

## Overview

Welcome to the README for our remarkable app, Rate My Professor This document offers a comprehensive introduction to the various screens and features of our application.

## Screenshots

**Homescreen**
<p align="center">
  <img src="/images/Homescreen.png" alt="Homescreen" width="200"/>
</p>
The Homescreen welcomes users with an engaging star animation.

---

**App Drawer**
<p align="center">
  <img src="/images/AppDrawer.png" alt="App Drawer" width="200"/>
</p>
The app drawer provides easy navigation within the application.

---

**Profiles**
<p align="center">
  <img src="/images/Profiles.png" alt="Profiles" width="200"/>
</p>
The Profiles screen displays all your professors and features an "Add Professors" button, enabling you to add and rate your professors.

---

**Details View**
<p align="center">
  <img src="/images/DetailsView.png" alt="Details View" width="200"/>
</p>
After adding your professors, the Details View is accessible by selecting a specific professor. It displays their Name, Email, Courses, Rating, and a hyperlink to the university's site, allowing you to access more information about your professor. Additionally, the Details View provides an option to edit professor information if needed.

---

**Add Professors**
<p align="center">
  <img src="/images/AddProfessors.png" alt="Add Professors" width="200"/>
</p>
The "Add Professors" feature enables you to input your desired professor's information, including name, email, courses, and rating.

---

**Edit Professors**
<p align="center">
  <img src="/images/EditProfessors.png" alt="Edit Professors" width="200"/>
</p>
The "Edit Professors" functionality is available within the Details View, allowing you to make corrections or adjust the rating for your professors.

---

**Settings**
<p align="center">
  <img src="/images/Settings.png" alt="Settings" width="200"/>
</p>
The "Settings" section provides you with the option to change the university associated with your professors, allowing you to link to the university of your choice.

---

## Installation

The installation is done trough the Android Studio IDE

---
